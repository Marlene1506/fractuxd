create procedure ActualizarFacturas @CodCli int

as

declare @NumFac int

select @NumFac = MAX(NumFac) from Facturas

if @NumFac is null set @NumFac=0
set @NumFac = @NumFac+1

insert into Facturas (NumFac,FecFac,CodCli) values (@NumFac,GETDATE(),@CodCli)

select * FROM Facturas where NumFac = @NumFac