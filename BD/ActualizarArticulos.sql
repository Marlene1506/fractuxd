USE [Administracion]
GO
/****** Object:  StoredProcedure [dbo].[ActualizarArticulos]    Script Date: 04/02/2019 11:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[ActualizarArticulos]

@Id_pro int,@Nom_pro varchar(100),@Precio float

as

--Actualiza articulos

if NOT EXISTS (SELECT Id_producto FROM Articulo where Id_producto=@Id_pro)
insert into Articulo (Id_producto,Nom_pro,Precio) values (@Id_pro,@Nom_pro,@Precio)

else

update Articulo set Id_producto=@Id_pro, Nom_pro=@Nom_pro, Precio=@Precio
