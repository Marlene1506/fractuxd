create procedure Datosfactura @NumFac int

as

select
F.*,D.PrecioVen,D.CanVen,C.Nom_cli,A.Nom_pro,D.PrecioVen*D.CanVen as importe

FROM 
Facturas F inner join Detalles D on F.NumFac =D.NumFac
inner join Articulo A on D.CodPro = A.Id_producto
inner join Cliente C on F.CodCli = C.id_clientes

where F.NumFac = @NumFac

